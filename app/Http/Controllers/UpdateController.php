<?php

namespace App\Http\Controllers;

use DB;

class UpdateController extends Controller
{
    public function update()
    {
        $truncate = DB::connection('default')->statement("TRUNCATE TABLE users_detail_pak, users_detail, dupak_detail, dupak_penilai");

        if ($truncate) {
            $ud = DB::connection('default')->statement("INSERT INTO users_detail (
                id_users,
                id_pak,
                tahun_dibuat,
                tgl_dibuat,
                periode,
                nama_lengkap,
                nip,
                karpeg,
                tempat_lahir,
                jk,
                tanggal_lahir,
                pangkat,
                gol_ruang,
                jabatan,
                nama_strata,
                nama_jurusan,
                unit_kerja,
                tmt,
                rn,
                tmt_jabatan) 
            SELECT * FROM dblink('test_server','SELECT * FROM spak.data_perorangan dp')
            AS tbi3 (
            id_users varchar,
            id_pak varchar,
            tahun_dibuat float,
            tgl_dibuat timestamp,
            periode varchar,
            nama_lengkap varchar,
            nip varchar,
            karpeg varchar,
            tempat_lahir varchar,
            jk varchar,
            tanggal_lahir varchar,
            pangkat varchar,
            gol_ruang varchar,
            jabatan varchar,
            nama_strata varchar,
            nama_jurusan varchar,
            unit_kerja varchar,
            tmt text,
            rn integer,
            tmt_jabatan text)");

            $udp = DB::connection('default')->statement("INSERT INTO users_detail_pak (
                id_pak,
                gol_ruang,
                ak_minimal,
                unsur,
                group_unsur,
                urutan,
                masa_penilaian,
                nomor_pak,
                hasil,
                nip_signer,
                signer,
                tembusan,
                tgl_penetapan)
                SELECT * FROM dblink('test_server','SELECT * FROM spak.data_detail_pak ddp') 
                AS tbi2 (id_pak VARCHAR,
                    gol_ruang VARCHAR,
                    ak_minimal integer,
                    unsur VARCHAR,
                    group_unsur VARCHAR,
                    urutan integer,
                    masa_penilaian VARCHAR,
                    nomor_pak VARCHAR,
                    hasil VARCHAR,
                    nip_signer VARCHAR,
                    signer VARCHAR,
                    tembusan VARCHAR,
                    tgl_penetapan text)");

            $dd = DB::connection('default')->statement("INSERT INTO dupak_detail (
                id_pak,
                nip,
                nama_lengkap,
                periode,
                pengajuan,
                id_penilai,
                penilaian) 
                SELECT * FROM dblink('test_server','SELECT * FROM spak.detail_dupak')
                    AS tbi4 (
                    id_pak varchar,
                    nip varchar,
                    nama_lengkap varchar,
                    periode varchar,
                    pengajuan numeric,
                    id_penilai varchar,
                    penilaian numeric)");

            $dp = DB::connection('default')->statement("INSERT INTO dupak_penilai (
                id_penilai,
                penilai,
                id_periode,
                periode,
                nip,
                jabatan,
                gol_ruang,
                unit_kerja) 
                SELECT * FROM dblink('test_server','SELECT * FROM spak.penilai_dupak')
                    AS tbi4 (
                    id_penilai varchar,
                    penilai varchar,
                    id_periode varchar,
                    periode varchar,
                    nip varchar,
                    jabatan varchar,
                    gol_ruang varchar,
                    unit_kerja varchar)");

            if ($udp || $ud || $dd || $dp) {
                echo "Update Berhasil";
            }
        }
    }
}
