<?php

namespace App\Http\Controllers;

use App\Model\DataDetailPak;
use App\Model\UsersDetail;
use App\Model\EvaluasiHistory;
use DB;

class QueryController extends Controller
{
    // DB::enableQueryLog();
    // dd(DB::getQueryLog());
    public function test()
    {
        $utama = DataDetailPak::select('urutan', 'group_unsur')
            ->whereIn('urutan', [3, 4, 6])
            ->distinct()
            ->orderBy('urutan', 'asc')
            ->get();

        $header = DataDetailPak::select('nomor_pak', 'masa_penilaian', 'hasil')
            ->where('id_pak', '402887bb73dbfca601783f5584be2fa3')
            ->first();

        $penunjang = DataDetailPak::where('id_pak', '402887bb66f5eb5f016d6768c0311d84')
            ->where('unsur', 'Unsur Penunjang')
            ->distinct()
            ->first();

        $pegawai = UsersDetail::where('nama_lengkap', 'LIKE', "%" . 'Wahyu' . "%")
            ->where('tahun_dibuat', '2020')
            ->distinct()
            ->orderBy('id_pak', 'desc')
            ->get();

        $ang_kre =  DataDetailPak::select('ak_minimal', 'nilai_unsur')
            ->join('user_angkre', 'user_angkre.id_users_pak', '=', 'data_detail_pak.id_pak')
            ->where('id_pak', '402887bb73dbfce20179a1e5e3102b90')
            ->where('user_angkre.unsur', 'Unsur Utama')
            ->distinct()
            ->orderBy('ak_minimal', 'desc')
            ->limit(1)
            ->get();

        foreach ($utama as $key => $value) {
            $grup  = $value['group_unsur'];

            foreach ($ang_kre as $ak) {
                $ak_minimal = $ak['ak_minimal'];
                $nilai_unsur = $ak['nilai_unsur'];
                $total = 0;
                $skor = array();

                if ($grup == "Tata Kelola dan Tata Laksana Teknologi Informasi") {
                    $skor[] = ($nilai_unsur - $ak_minimal) * 0.2;
                } elseif ($grup == "Infrastruktur Teknologi Informasi") {
                    $skor[] = ($nilai_unsur - $ak_minimal) * 0.5;
                } else {
                    $skor[] = ($nilai_unsur - $ak_minimal) * 0.3;
                }

                // echo $skor."\n";
                print_r($skor);
            }
        }


        // echo $ang_kre->ak_minimal;
        echo (json_encode($utama)) . "\n\n\n";
        echo (json_encode($ang_kre) . "\n\n\n");
        // echo (json_encode($penunjang));
    }

    public function old()
    {
        $cek = DB::select("SELECT eph.id_users_pak, COUNT(urutan) AS jml 
        FROM evaluasi_pak_history eph 
        JOIN users_pak up ON eph.id_users_pak = up.id 
        WHERE up.id_masa_pak = '402887bb73dbfce2017b2498ecf93cd2' and eph.id_users_pak = '402887bb73dbfca6017848fe260f2fc4'
        GROUP BY id_users_pak
        HAVING COUNT(urutan) = 8");

        // $cek = DB::connection('default')->select("SELECT eph.id_users_pak, COUNT(urutan) AS jml 
        //         FROM evaluasi_pak_history eph 
        //         JOIN users_pak up ON eph.id_users_pak = up.id 
        //         WHERE up.id_masa_pak = '402887bb73dbfce2017b2498ecf93cd2' and eph.id_users_pak = '402887bb73dbfce2017c10ac903f44bb'
        //         GROUP BY id_users_pak
        //         HAVING COUNT(urutan) = 8");

        if (json_encode($cek)) {
            echo "bisa";
        } else {
            echo "gabisa";
        }
        // DB::connection('prod')->enableQueryLog();

        // $cek = DB::connection('prod')->table('evaluasi_pak_history')
        //     ->select(DB::raw('evaluasi_pak_history.id_users_pak, count(urutan) AS jml'))
        //     ->join('users_pak', 'users_pak.id', '=', 'evaluasi_pak_history.id_users_pak')
        //     ->where('users_pak.id_masa_pak', '402887bb73dbfce2017b2498ecf93cd2')
        //     ->where('evaluasi_pak_history.id_users_pak', '402887bb73dbfce2017c10ac903f44bb')
        //     ->groupBy('id_users_pak')
        //     ->havingRaw('COUNT(urutan) = ?', [8])
        //     ->get();
        // dd(DB::getQueryLog($cek));


        // DB::getQueryLog($cek);
        echo json_encode($cek);

        // $cek = EvaluasiHistory::select('nama_lengkap', 'unsur', 'jumlah')
        //     ->join('data_perorangan', 'data_perorangan.id_pak', '=', 'evaluasi_pak_history.id_users_pak')
        //     ->where('data_perorangan.periode', '=', 'PAK Semester II 2020')
        //     ->where('urutan', 9)
        //     ->where('nama_lengkap', 'Ardi Sulistyo Widodo')
        //     ->first();

        // print_r($cek);
    }
}
