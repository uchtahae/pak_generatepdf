<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Handle an authentication attempt.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return Response
     */

    public function login()
    {
        return view('page.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        try {
            $client = new Client();
            $response = $client->request('POST', 'https://map.bpkp.go.id/api/v3/login', [
                'headers' => ['Content-type' => 'application/x-www-form-urlencoded'],
                'form_params' => [
                    'username' => $request->username,
                    'password' => $request->password,
                    'kelas_user' => 0
                ]
            ]);

            if ($response->getBody()) {
                $request->session()->regenerate();
                return redirect()->intended('/form');
            }
        } catch (BadResponseException  $e) {
            $response = $e->getResponse();

            if ($response->getBody()) {
                return redirect('/login')->with(['danger' => 'Login Gagal ! Periksa kembali username dan password.']);
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
