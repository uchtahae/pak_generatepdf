<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\UsersDetail;
use App\Model\DataDetailPak;
use App\Model\DetailDupak;
use App\Model\PenilaiDupak;
use App\Model\UserAK;
use App\Model\LastAK;

use DB;
use Fpdf;

class DownloadController extends Controller
{
    public function pak($id_pak)
    {
        $data_pak = UsersDetail::where('id_pak', $id_pak)
            ->first();

        $header = DataDetailPak::select('nomor_pak', 'masa_penilaian', 'hasil')
            ->where('id_pak', $id_pak)
            ->first();

        header("Content-type:application/pdf");
        // $date = date('Y-m-d_H:i:s');

        $pdf = new Fpdf('P', 'mm', 'A4');

        $pdf::setFillColor(255, 255, 255);

        $pdf::AddPage();
        $pdf::SetFont('Arial', '', 10);
        $pdf::Cell(0, 5, "BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(0, 5, "PENETAPAN ANGKA KREDIT JABATAN FUNGSIONAL PRANATA KOMPUTER", 0, "", "C");
        $pdf::Ln();

        $pdf::Cell(0, 5, "{$header->nomor_pak}", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(0, 5, "{$header->masa_penilaian}", 0, "", "C");
        $pdf::Ln();

        $pdf::SetFont('Arial', '', 10);
        $pdf::Cell(60, 6, " ", "LT", 0, "L", 0);
        $pdf::Cell(66, 6, "KETERANGAN PERORANGAN", "T", 0, "C", 0);
        $pdf::Cell(65, 6, " ", "RT", 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "1.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Nama", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->nama_lengkap}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "2.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "NIP", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->nip}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "3.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Nomor Seri Karpeg", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->karpeg}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "4.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Pangkat/Gol.Ruang/TMT", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->pangkat}, {$data_pak->gol_ruang}, {$data_pak->tmt} ", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "5.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Tempat dan Tanggal Lahir", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->tempat_lahir}, {$data_pak->tanggal_lahir}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "6.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Jenis Kelamin", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->jk}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "7.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Pendidikan Tertinggi", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->nama_strata}, {$data_pak->nama_jurusan}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "8.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Jabatan Fungsional/TMT", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->jabatan}, {$data_pak->tmt_jabatan}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "9.", 1, 0, "L", 0);
        $pdf::Cell(83, 6, "Unit Kerja", 1, 0, "L", 0);
        $pdf::Cell(98, 6, "{$data_pak->unit_kerja}", 1, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(191, 2, " ", "LR", 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(191, 6, "PENETAPAN ANGKA KREDIT", 1, 0, "C", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "No.", 1, 0, "C", 0);
        $pdf::Cell(146, 6, "URAIAN", 1, 0, "C", 0);
        $pdf::Cell(35, 6, "JUMLAH", 1, 0, "C", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "(1)", 1, 0, "C", 0);
        $pdf::Cell(146, 6, "(2)", 1, 0, "C", 0);
        $pdf::Cell(35, 6, "", 1, 0, "R", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "A.", "LR", 0, "C", 0);
        $pdf::Cell(146, 6, "UNSUR UTAMA", 1, 0, "L", 0);
        $pdf::Cell(35, 6, "", 1, 0, "R", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "", "LR", 0, "R", 0);
        $pdf::Cell(146, 6, "1. Tugas Jabatan", 1, 0, "L", 0);
        $pdf::Cell(35, 6, "", 1, 0, "R", 0);
        $pdf::Ln();

        $utama = DataDetailPak::select('urutan', 'group_unsur')
            ->whereIn('urutan', [3, 4, 6])
            ->distinct()
            ->orderBy('urutan', 'asc')
            ->get();

        $i = "a";
        $total_tugas_jabatan = 0;
        foreach ($utama as $key => $value) {
            $abjad = $i++;
            $grup  = $value['group_unsur'];

            if ($grup == "Pengembangan Profesi") {
                break;
            }

            //tkti = (angkre- minimal x 0.2)
            //infra = (angkre- minimal x 0.5)
            //si = (angkre- minimal x 0.3)
            $pdf::Cell(10, 6, "", "LR", 0, "R", 0);
            $pdf::Cell(6, 6, "", "B", 0, "L", 0);
            $pdf::Cell(140, 6, "{$abjad}. {$grup}", "B", 0, "L", 0);

            $ang_kre = DataDetailPak::select('ak_minimal', 'nilai_unsur')
                ->join('user_angkre', 'user_angkre.id_users_pak', '=', 'users_detail_pak.id_pak')
                ->where('id_pak', $id_pak)
                ->where('user_angkre.unsur', 'Unsur Utama')
                ->distinct()
                ->orderBy('ak_minimal', 'desc')
                ->limit(1)
                ->get();

            foreach ($ang_kre as $ak) {
                $ak_minimal = $ak['ak_minimal'];
                $nilai_unsur = $ak['nilai_unsur'];
                $total = 0;
                $skor = 0;

                if ($grup == "Tata Kelola dan Tata Laksana Teknologi Informasi") {
                    $skor = ($nilai_unsur - $ak_minimal) * 0.2;
                } elseif ($grup == "Infrastruktur Teknologi Informasi") {
                    $skor = ($nilai_unsur - $ak_minimal) * 0.5;
                } else {
                    $skor = ($nilai_unsur - $ak_minimal) * 0.3;
                }

                $skor_format = number_format($skor, 3);

                $pdf::Cell(35, 6, "{$skor_format}", 1, 0, "C", 0);
                $pdf::Ln();
            }
            $total_tugas_jabatan += $skor;
        }

        $ttjFormat = number_format($total_tugas_jabatan, 3);
        $pdf::Cell(10, 6, "", "LR", 0, "R", 0);
        $pdf::Cell(146, 6, "JUMLAH TUGAS JABATAN", 1, 0, "C", 0);
        $pdf::Cell(35, 6, "{$ttjFormat}", 1, 0, "C", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "", "LR", 0, "R", 0);
        $pdf::Cell(146, 6, "2. Pengembangan Profesi", 1, 0, "L", 0);
        $pdf::Cell(35, 6, "-", 1, 0, "C", 0);
        $pdf::Ln();

        $pdf::Cell(10, 6, "", "LR", 0, "R", 0);
        $pdf::Cell(146, 6, "JUMLAH UNSUR UTAMA ( 1 + 2 )", 1, 0, "C", 0);
        $pdf::Cell(35, 6, "{$ttjFormat}", 1, 0, "C", 0);
        $pdf::Ln();

        $penunjang = UserAK::select('nilai_unsur')
            ->where('id_users_pak', $id_pak)
            ->where('unsur', 'Unsur Penunjang')
            ->first();

        $gabungan = $penunjang->nilai_unsur;

        $cek = DB::select("SELECT eph.id_users_pak, COUNT(urutan) AS jml 
                FROM evaluasi_pak_history eph 
                JOIN users_pak up ON eph.id_users_pak = up.id 
                WHERE up.id_masa_pak = '402887bb73dbfce2017b2498ecf93cd2' and eph.id_users_pak = '" . $id_pak . "'
                GROUP BY id_users_pak
                HAVING COUNT(urutan) = 8");

        if ($cek) {
            $ak_lama = LastAK::select('unsur', 'jumlah')
                ->where('nama_lengkap', $data_pak->nama_lengkap)
                ->first();

            if (!empty($ak_lama)) {
                $gabungan = $penunjang->nilai_unsur + $ak_lama->jumlah;
            }
        }

        $pdf::Cell(10, 6, "B.", 1, 0, "C", 0);
        $pdf::Cell(146, 6, "UNSUR PENUNJANG", 1, 0, "L", 0);
        $pdf::Cell(35, 6, "{$gabungan}", 1, 0, "C", 0);
        $pdf::Ln();

        $total = $total_tugas_jabatan + $gabungan;

        $pdf::Cell(156, 6, "JUMLAH ( A + B )", 1, 0, "C", 0);
        $pdf::Cell(35, 6, " {$total}", 1, 0, "C", 0);
        $pdf::Ln();

        $sign = DataDetailPak::select('hasil', 'tgl_penetapan', 'signer', 'nip_signer', 'tembusan')
            ->where('id_pak', $id_pak)
            ->distinct()
            ->first();

        $pdf::Cell(191, 6, "REKOMENDASI", 1, 0, "C", 0);
        $pdf::Ln();
        $pdf::MultiCell(191, 6, "{$sign->hasil}", "LRTB", 1, 0, "L", 0);
        $pdf::Ln();


        $pdf::SetFont('Arial', '', 9);
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::Cell(60, 4, "Ditetapkan di: Jakarta", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::Cell(60, 4, "{$sign->tgl_penetapan}", 0, "", "L");
        $pdf::Ln();
        $pdf::SetFont('Arial', 'I', 9);
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::SetTextColor(133, 131, 131);
        $pdf::Cell(60, 4, "Ditandatangani secara elektronik oleh", 0, "", "L");
        $pdf::Ln();
        $pdf::SetTextColor(0, 0, 0);
        $pdf::SetFont('Arial', '', 9);
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::Cell(60, 4, "Kepala Pusat,", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::Cell(60, 4, "{$sign->signer}", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(115, 4, "", 0, "", "L");
        $pdf::Cell(60, 4, "NIP. {$sign->nip_signer}", 0, "", "L");

        $pdf::Ln();
        $pdf::Cell(0, 5, "Tembusan :", 0, "", "L");
        $pdf::Ln();
        if ($data_pak->unit_kerja === 'Pusat Informasi Pengawasan') {

            $pdf::Cell(0, 5, "1. Kepala Badan Pusat Statistik", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "cq. Kepala Bidang Pembinaan Jabatan Fungsional Pranata Komputer;", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "2. {$sign->tembusan};", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "3. Pranata Komputer yang Bersangkutan;", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "4. Arsip.", 0, "", "L");
        } elseif ($data_pak->unit_kerja === 'Biro Sumber Daya Manusia') {

            $pdf::Cell(0, 5, "1. Kepala Badan Pusat Statistik", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "cq. Kepala Bidang Pembinaan Jabatan Fungsional Pranata Komputer;", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "2. {$sign->tembusan};", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "3. Pranata Komputer yang Bersangkutan;", 0, "", "L");
            $pdf::Ln();
            $pdf::Cell(0, 5, "4. Arsip.", 0, "", "L");
        } else {

            if (str_contains($data_pak->unit_kerja, 'Deputi')) {

                $pdf::Cell(0, 5, "1. {$sign->tembusan};", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "2. Kepala Badan Pusat Statistik", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "cq. Kepala Bidang Pembinaan Jabatan Fungsional Pranata Komputer;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "3. Kepala Biro Sumber Daya Manusia;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "4. Pranata Komputer yang Bersangkutan;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "5. Arsip.", 0, "", "L");
            } else {

                $pdf::Cell(0, 5, "1. Kepala Badan Pusat Statistik", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "cq. Kepala Bidang Pembinaan Jabatan Fungsional Pranata Komputer;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "2. Kepala Biro Sumber Daya Manusia;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "3. {$sign->tembusan};", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "4. Pranata Komputer yang Bersangkutan;", 0, "", "L");
                $pdf::Ln();
                $pdf::Cell(0, 5, "5. Arsip.", 0, "", "L");
            }
        }

        $pdf::Ln();
        $pdf::Ln();
        $pdf::Ln();

        $pdf::Image(public_path('img/bse.png'), 30, 276, -500);
        $pdf::Image(public_path('img/kalimat_bse.png'), 45, 277, -150);

        $pdf::Output("PAK_{$data_pak->nama_lengkap}.pdf", "D");
        exit;
    }

    public function suket(Request $request)
    {
        $request->validate([
            'no_suket' => 'required'
        ]);

        $penilai = PenilaiDupak::select('penilai', 'periode', 'nip', 'gol_ruang', 'jabatan', 'unit_kerja')
            ->where('id_penilai', $request->id_penilai)
            ->where('id_periode', $request->id_periode)
            ->first();

        $detail = DetailDupak::select('nip', 'nama_lengkap', 'pengajuan', 'penilaian')
            ->where('id_penilai', $request->id_penilai)
            ->where('periode', $request->id_periode)
            ->get();

        header("Content-type:application/pdf");

        $pdf = new Fpdf('P', 'mm', 'A4');

        $pdf::setFillColor(255, 255, 255);

        $pdf::AddPage();
        $pdf::SetFont('Arial', 'B', 12);

        $pdf::Image(public_path('img/BPKP_Logo.png'), 30, 12, 28, 15);
        $pdf::Cell(60, 5, "", 0, "", "C");
        $pdf::Cell(100, 5, "BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(60, 5, "", 0, "", "C");
        $pdf::Cell(100, 5, "PUSAT INFORMASI PENGAWASAN", 0, "", "C");
        $pdf::Ln();
        $pdf::SetFont('Arial', '', 11);
        $pdf::Cell(60, 5, "", 0, "", "C");
        $pdf::Cell(100, 5, "Jalan Pramuka Nomor 33 Lantai 7, Jakarta 13120", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(60, 5, "", 0, "", "C");
        $pdf::Cell(100, 5, "Telepon: (021) 85910031 (Hunting), Faksimile: (021) 85910208", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(60, 5, "", 0, "", "C");
        $pdf::Cell(100, 5, "Surel: pusinfowas@bpkp.go.id", 0, "", "C");
        $pdf::Ln();
        $pdf::SetFont('Arial', 'B', 12);
        $pdf::Cell(20, 3, "", 0, "", "C");
        $pdf::Cell(150, 3, "", "B", 0, "C", 0);
        $pdf::Ln();
        $pdf::Cell(0, 7, "SURAT KETERANGAN", 0, "", "C");
        $pdf::Ln();
        $pdf::Cell(0, 5, "{$request->no_suket}", 0, "", "C");
        $pdf::Ln();

        $pdf::SetFont('Arial', '', 11);
        $pdf::Cell(20, 5, "", 0, "", "C");
        $pdf::Cell(50, 5, "Ketua Tim Penilai Angka kredit Jabatan Fungsional Pranata Komputer (JFPK) dengan", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(20, 5, "", 0, "", "C");
        $pdf::Cell(50, 5, "ini menerangkan bahwa:", 0, "", "L");
        $pdf::Ln();

        $pdf::Cell(25, 5, "", 0, 0, "L", 0);
        $pdf::Cell(30, 5, "Nama", 0, 0, "L", 0);
        $pdf::Cell(10, 5, ":", 0, 0, "C", 0);
        $pdf::Cell(30, 5, "{$penilai->penilai}", 0, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(25, 5, "", 0, 0, "L", 0);
        $pdf::Cell(30, 5, "NIP", 0, 0, "L", 0);
        $pdf::Cell(10, 5, ":", 0, 0, "C", 0);
        $pdf::Cell(30, 5, "{$penilai->nip}", 0, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(25, 5, "", 0, 0, "L", 0);
        $pdf::Cell(30, 5, "Pangkat/Gol", 0, 0, "L", 0);
        $pdf::Cell(10, 5, ":", 0, 0, "C", 0);
        $pdf::Cell(30, 5, "{$penilai->gol_ruang}", 0, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(25, 5, "", 0, 0, "L", 0);
        $pdf::Cell(30, 5, "Jabatan", 0, 0, "L", 0);
        $pdf::Cell(10, 5, ":", 0, 0, "C", 0);
        $pdf::Cell(100, 5, "{$penilai->jabatan}", 0, 0, "L", 0);
        $pdf::Ln();

        $pdf::Cell(25, 5, "", 0, 0, "L", 0);
        $pdf::Cell(30, 5, "Unit Kerja", 0, 0, "L", 0);
        $pdf::Cell(10, 5, ":", 0, 0, "C", 0);
        $pdf::Cell(30, 5, "{$penilai->unit_kerja}", 0, 0, "L", 0);
        $pdf::Ln();
        $pdf::Ln();

        $pdf::Cell(20, 5, "", 0, 0, "L", 0);
        $pdf::Cell(50, 5, "telah melakukan penilaian DU{$penilai->periode} untuk pegawai sebagai berikut:", 0, "", "L");
        $pdf::Ln();

        $pdf::SetFont('Arial', '', 10);
        $pdf::Cell(22, 5, "", 0, 0, "L", 0);
        $pdf::Cell(10, 5, "No", 1, 0, "L", 0);
        $pdf::Cell(50, 5, "NIP", 1, 0, "L", 0);
        $pdf::Cell(55, 5, "Nama", 1, 0, "L", 0);
        $pdf::Cell(20, 5, "Pengajuan", 1, 0, "L", 0);
        $pdf::Cell(20, 5, "Penilaian", 1, 0, "L", 0);
        $pdf::Ln();

        $no = 1;
        foreach ($detail as $dtl) {
            $pengajuan = number_format($dtl->pengajuan, 3, ".", "");
            $penilaian = number_format($dtl->penilaian, 3, ".", "");

            $pdf::Cell(22, 5, "", 0, 0, "L", 0);
            $pdf::Cell(10, 5, "{$no}", 1, 0, "L", 0);
            $pdf::Cell(50, 5, "{$dtl->nip} ", 1, 0, "L", 0);
            $pdf::Cell(55, 5, "{$dtl->nama_lengkap}", 1, 0, "L", 0);
            $pdf::Cell(20, 5, "$pengajuan", 1, 0, "L", 0);
            $pdf::Cell(20, 5, "$penilaian", 1, 0, "L", 0);
            $pdf::Ln();

            $no++;
        }

        $pdf::Ln();
        $pdf::Cell(20, 5, "", 0, 0, "L", 0);
        $pdf::SetFont('Arial', '', 11);
        $pdf::Cell(40, 5, "Demikian surat keterangan ini untuk dipergunakan sebagaimana mestinya.", 0, "", "L");
        $pdf::Ln();

        $pdf::Cell(120, 6, "", 0, "", "L");
        $pdf::Cell(60, 6, "Jakarta, 30 September 2021", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(120, 6, "", 0, "", "L");
        $pdf::Cell(60, 6, "Ketua Tim Penilai,", 0, "", "L");
        $pdf::Ln();
        $pdf::SetFont('Arial', 'I', 10);
        $pdf::Cell(120, 6, "", 0, "", "L");
        $pdf::SetTextColor(133, 131, 131);
        $pdf::Cell(60, 6, "Ditandatangani secara elektronik oleh", 0, "", "L");
        $pdf::Ln();
        $pdf::SetFont('Arial', '', 11);
        $pdf::SetTextColor(0, 0, 0);
        $pdf::Cell(120, 6, "", 0, "", "L");
        $pdf::Cell(60, 6, "Padmono Fery Nurtjahjo", 0, "", "L");
        $pdf::Ln();
        $pdf::Cell(120, 6, "", 0, "", "L");
        $pdf::Cell(60, 6, "NIP 19690224 199003 1 001", 0, "", "L");
        $pdf::Ln();

        $pdf::Image(public_path('img/bse-dupak.png'), 28, 275, 160, 15);

        $pdf::Output("SuKetPenilai_{$penilai->penilai}.pdf", "D");
        exit;
    }
}
