<?php

namespace App\Http\Controllers;

use Fpdf;

class PdfController extends Controller
{

    public function tester()
    {
        header("Content-type:application/pdf");
        $date = date('Y-m-d_H:i:s');

        $pdf = new Fpdf('P','mm','A4');

        $pdf::setFillColor(255,255,255);

        $pdf::AddPage();
        $pdf::SetFont('Arial','',10);
        $pdf::Cell(0,5,"BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN",0,"","C");
        $pdf::Ln();
        $pdf::Cell(0,5,"PENETAPAN ANGKA KREDIT JABATAN FUNGSIONAL PRANATA KOMPUTER",0,"","C");
        $pdf::Ln();
        $pdf::Cell(0,5,"Nomor: KEP-292/IP/2021",0,"","C");
        $pdf::Ln();
        $pdf::Cell(0,5,"Masa Penilaian 1 Januari s.d 30 Juni 2021",0,"","C");
        $pdf::Ln();

        $pdf::SetFont('Arial','',10);
        $pdf::Cell(191,6,"KETERANGAN PERORANGAN",1,0,"C",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"1.",1,0,"L",0);
        $pdf::Cell(83,6,"Nama",1,0,"L",0);
        $pdf::Cell(98,6,"Dinul Haniffatma",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"2.",1,0,"L",0);
        $pdf::Cell(83,6,"NIP",1,0,"L",0);
        $pdf::Cell(98,6,"19930329 201801 2 001",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"3.",1,0,"L",0);
        $pdf::Cell(83,6,"Nomor Seri Karpeg",1,0,"L",0);
        $pdf::Cell(98,6,"-",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"4.",1,0,"L",0);
        $pdf::Cell(83,6,"Pangkat/Gol.Ruang/TMT",1,0,"L",0);
        $pdf::Cell(98,6,"Pengatur, II/c, 1 Januari 2019",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"5.",1,0,"L",0);
        $pdf::Cell(83,6,"Tempat dan Tanggal Lahir",1,0,"L",0);
        $pdf::Cell(98,6,"Padang, 29 Maret 1993",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"6.",1,0,"L",0);
        $pdf::Cell(83,6,"Jenis Kelamin",1,0,"L",0);
        $pdf::Cell(98,6,"Perempuan",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"7.",1,0,"L",0);
        $pdf::Cell(83,6,"Pendidikan Tertinggi",1,0,"L",0);
        $pdf::Cell(98,6,"D.III, Teknik Informatika",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"8.",1,0,"L",0);
        $pdf::Cell(83,6,"Jabatan Fungsional/TMT",1,0,"L",0);
        $pdf::Cell(98,6,"Pranata Komputer Pelaksana / 1 Januari 2019",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"9.",1,0,"L",0);
        $pdf::Cell(83,6,"Unit Kerja",1,0,"L",0);
        $pdf::Cell(98,6,"Pusat Informasi Pengawasan",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(191,2," ","LR",0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(191,6,"PENETAPAN ANGKA KREDIT",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"No.",1,0,"C",0);
        $pdf::Cell(146,6,"URAIAN",1,0,"C",0);
        $pdf::Cell(35,6,"JUMLAH",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"(1)",1,0,"C",0);
        $pdf::Cell(146,6,"(2)",1,0,"C",0);
        $pdf::Cell(35,6,"",1,0,"R",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"A.","LR",0,"C",0);
        $pdf::Cell(146,6,"UNSUR UTAMA",1,0,"L",0);
        $pdf::Cell(35,6,"",1,0,"R",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"1. Tugas Jabatan",1,0,"L",0);
        $pdf::Cell(35,6,"",1,0,"R",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"a. Tata Kelola dan Tata Laksana Teknologi Informasi",1,0,"L",0);
        $pdf::Cell(35,6,"18,000",1,0,"C",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"b. Infrastruktur Teknologi Informasi",1,0,"L",0);
        $pdf::Cell(35,6,"45,000",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"c. Sistem Informasi dan Multimedia",1,0,"L",0);
        $pdf::Cell(35,6,"27,000",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"JUMLAH TUGAS JABATAN",1,0,"C",0);
        $pdf::Cell(35,6,"90,000",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"2. Pengembangan Profesi",1,0,"L",0);
        $pdf::Cell(35,6,"-",1,0,"C",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"","LR",0,"R",0);
        $pdf::Cell(146,6,"JUMLAH UNSUR UTAMA",1,0,"C",0);
        $pdf::Cell(35,6,"90,000",1,0,"C",0);
        $pdf::Ln();

        $pdf::Cell(10,6,"B.",1,0,"C",0);
        $pdf::Cell(146,6,"UNSUR PENUNJANG",1,0,"L",0);
        $pdf::Cell(35,6,"5,000",1,0,"C",0);
        $pdf::Ln();

        $pdf::Cell(156,6,"JUMLAH ( A + B )",1,0,"C",0);
        $pdf::Cell(35,6,"95,000",1,0,"C",0);
        $pdf::Ln();
        
        $pdf::Cell(191,6,"REKOMENDASI",1,0,"C",0);
        $pdf::Ln();
        $pdf::MultiCell(191,6,"Dapat Dipertimbangkan untuk dinaikkan dalam Jabatan Pranata Komputer Ahli Muda dan Pangkat/Golongan Ruang Penata (III/c) dengan angka kredit sebesar 100, dengan melampirkan sertifikat lulus uji kompetensi. Untuk PAK berikutnya, angka kredit dimulai dari 0.","LRTB",1,0,"L",0);
        $pdf::Ln();
        
        $pdf::SetFont('Arial','',9);
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Ditetapkan di: Jakarta",0,"","L");
        $pdf::Ln();
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Pada tanggal: 30 Agustus 2021",0,"","L");
        $pdf::Ln();
        $pdf::SetFont('Arial','I',9);
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::SetTextColor(133,131,131);
        $pdf::Cell(60,6,"Ditandatangani secara elektronik oleh",0,"","L");
        $pdf::Ln();
        $pdf::SetTextColor(0,0,0);
        $pdf::SetFont('Arial','',9);
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Kepala Pusat,",0,"","L");
        $pdf::Ln();
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Agus Puruhitaarga Purnomo Widodo",0,"","L");
        $pdf::Ln();
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"NIP 19680809 198903 1 001",0,"","L");

        $pdf::Ln();
        $pdf::Ln();
        $pdf::Cell(0,5,"Tembusan :",0,"","L");
        $pdf::Ln();
        $pdf::Cell(0,5,"1. Pranata Komputer yang Bersangkutan;",0,"","L");
        $pdf::Ln();
        $pdf::Cell(0,5,"2. Kepala Pusat Informasi Pengawasan;",0,"","L");
        $pdf::Ln();
        $pdf::Cell(0,5,"3. Kepala Badan Pusat Statistik;",0,"","L");
        $pdf::Ln();
        $pdf::Cell(0,5,"cq. Kepala Bidang Pembinaan Jabatan Fungsional Pranata Komputer;",0,"","L");
        $pdf::Ln();
        $pdf::Cell(0,5,"4. Arsip.",0,"","L");

        $pdf::Image(public_path('img/bse.png'), 30, 275, -500);
        $pdf::Image(public_path('img/kalimat_bse.png'), 45, 275, -150);

        $pdf::Output("PAK_{$date}.pdf", "D");
        exit;
    }

    public function test_suket() 
    {
        header("Content-type:application/pdf");
        $date = date('Y-m-d_H:i:s');

        $pdf = new Fpdf('P','mm','A4');

        $pdf::setFillColor(255,255,255);

        $pdf::AddPage();
        $pdf::SetFont('Arial','B',12);
        
        $pdf::Image(public_path('img/BPKP_Logo.png'), 30, 12, 28, 15);
        $pdf::Cell(60,4,"",0,"","C");
        $pdf::Cell(100,4,"BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN",0,"","C");
        $pdf::Ln();
        $pdf::Cell(60,4,"",0,"","C");
        $pdf::Cell(100,4,"PUSAT INFORMASI PENGAWASAN",0,"","C");
        $pdf::Ln();
        $pdf::SetFont('Arial','',11);
        $pdf::Cell(60,4,"",0,"","C");
        $pdf::Cell(100,4,"Jalan Pramuka Nomor 33 Lantai 7, Jakarta 13120",0,"","C");
        $pdf::Ln();
        $pdf::Cell(60,4,"",0,"","C");
        $pdf::Cell(100,4,"Telepon: (021) 85910031 (Hunting), Faksimile: (021) 85910208",0,"","C");
        $pdf::Ln();
        $pdf::Cell(60,4,"",0,"","C");
        $pdf::Cell(100,4,"Surel: pusinfowas@bpkp.go.id",0,"","C");
        $pdf::Ln();
        $pdf::SetFont('Arial','B',12);
        $pdf::Cell(20,3,"",0,"","C");
        $pdf::Cell(150,3,"","B",0,"C",0);
        $pdf::Ln();
        $pdf::Cell(0,7,"SURAT KETERANGAN",0,"","C");
        $pdf::Ln();
        $pdf::Cell(0,5,"NOMOR: KET-02/IP3/2021",0,"","C");
        $pdf::Ln();
        $pdf::Ln();
        
        $pdf::SetFont('Arial','',11);
        $pdf::Cell(20,5,"",0,"","C");
        $pdf::Cell(50,5,"Ketua Tim Penilai Angka kredit Jabatan Fungsional Pranata Komputer (JFPK) dengan",0,"","L");
        $pdf::Ln();
        $pdf::Cell(20,5,"",0,"","C");
        $pdf::Cell(50,5,"ini menerangkan bahwa:",0,"","L");
        $pdf::Ln();
        
        $pdf::Cell(25,5,"",0,0,"L",0);
        $pdf::Cell(30,5,"Nama",0,0,"L",0);
        $pdf::Cell(10,5,":",0,0,"C",0);
        $pdf::Cell(30,5,"Andi Irawan",0,0,"L",0);
        $pdf::Ln();

        $pdf::Cell(25,5,"",0,0,"L",0);
        $pdf::Cell(30,5,"NIP",0,0,"L",0);
        $pdf::Cell(10,5,":",0,0,"C",0);
        $pdf::Cell(30,5,"19800915 200501 1 001",0,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(25,5,"",0,0,"L",0);
        $pdf::Cell(30,5,"Pangkat/Gol",0,0,"L",0);
        $pdf::Cell(10,5,":",0,0,"C",0);
        $pdf::Cell(30,5,"III/c",0,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(25,5,"",0,0,"L",0);
        $pdf::Cell(30,5,"Jabatan",0,0,"L",0);
        $pdf::Cell(10,5,":",0,0,"C",0);
        $pdf::MultiCell(0,4,"Pranata Komputer Muda selaku Subkoordinator Keamanan Teknologi Informasi",0,0,"L",0);
        $pdf::Ln();
        
        $pdf::Cell(25,5,"",0,0,"L",0);
        $pdf::Cell(30,5,"Unit Kerja",0,0,"L",0);
        $pdf::Cell(10,5,":",0,0,"C",0);
        $pdf::Cell(30,5,"Pusat Informasi Pengawasan",0,0,"L",0);
        $pdf::Ln();
        $pdf::Ln();
        
        $pdf::Cell(20,5,"",0,0,"L",0);
        $pdf::Cell(50,5,"telah melakukan penilaian DUPAK semester II tahun 2020 untuk pegawai sebagai berikut:",0,"","L");
        $pdf::Ln();
        $pdf::Ln();
        
        $pdf::Cell(22,5,"",0,0,"L",0);
        $pdf::Cell(10,5,"No",1,0,"L",0);
        $pdf::Cell(50,5,"NIP",1,0,"L",0);
        $pdf::Cell(55,5,"Nama",1,0,"L",0);
        $pdf::Cell(20,5,"Sebelum",1,0,"L",0);
        $pdf::Cell(20,5,"Sesudah",1,0,"L",0);
        $pdf::Ln();
        
        for($i=1; $i<= 10; $i++)
        {
            $pdf::Cell(22,5,"",0,0,"L",0);
            $pdf::Cell(10,5,"$i",1,0,"L",0);
            $pdf::Cell(50,5,"NIP $i",1,0,"L",0);
            $pdf::Cell(55,5,"Nama $i",1,0,"L",0);
            $pdf::Cell(20,5,"PAK $i",1,0,"L",0);
            $pdf::Cell(20,5,"PAK $i",1,0,"L",0);
            $pdf::Ln();
        }
        
        $pdf::Ln();
        $pdf::Ln();
        $pdf::Cell(20,5,"",0,0,"L",0);
        $pdf::Cell(40,5,"Demikian surat keterangan ini untuk dipergunakan sebagaimana mestinya.",0,"","L");
        $pdf::Ln();
        $pdf::Ln();

        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Jakarta, 30 September 2021",0,"","L");
        $pdf::Ln();
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Ketua Tim Penilai,",0,"","L");
        $pdf::Ln();
        $pdf::SetFont('Arial','I',10);
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::SetTextColor(133,131,131);
        $pdf::Cell(60,6,"Ditandatangani secara elektronik oleh",0,"","L");
        $pdf::Ln();
        $pdf::SetFont('Arial','',11);
        $pdf::SetTextColor(0,0,0);
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"Padmono Fery Nurtjahjo",0,"","L");
        $pdf::Ln();
        $pdf::Cell(120,6,"",0,"","L");
        $pdf::Cell(60,6,"NIP 19690224 199003 1 001",0,"","L");
        $pdf::Ln();

        $pdf::Image(public_path('img/bse-dupak.png'), 28, 275, 160, 15);

        $pdf::Output("Dupak_{$date}.pdf", "D");
        exit;
    }
}
