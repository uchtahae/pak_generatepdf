<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Model\UsersDetail;
use App\Model\PenilaiDupak;

class PagesController extends Controller
{
    public function home(Request $request)
    {
        if ($request->session()->get('login') != null) {
            return redirect('/form');
        }
        return view('layout.default');
    }

    public function form()
    {
        return view('page.form');
    }

    public function dupak()
    {
        $periode = PenilaiDupak::select('periode')
            ->distinct()
            ->get();

        return view('page.dupak', compact('periode'));
    }

    public function search(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $request->flash();

        // DB::enableQueryLog();
        $pegawai = UsersDetail::where('nama_lengkap', '~*', $request->nama)
            ->orderBy('id_pak', 'desc')
            ->get();

        // dd(DB::getQueryLog());
        return view('page.form', ['pegawai' => $pegawai]);
    }

    public function search_dupak(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'periode' => 'required'
        ]);

        $request->flash(['nama', 'periode']);

        $periode = PenilaiDupak::select('periode')
            ->distinct()
            ->get();

        $penilai = PenilaiDupak::where('penilai', '~*', $request->nama)
            ->where('periode', $request->periode)
            ->orderBy('penilai', 'desc')
            ->get();

        return view('page.dupak', ['periode' => $periode, 'penilai' => $penilai]);
    }

    public function config()
    {
        try {
            DB::connection()->getPdo();
            if (DB::connection()->getDatabaseName()) {
                echo "Yes! Successfully connected to the DB: " . DB::connection()->getDatabaseName();
            } else {
                die("Could not find the database. Please check your configuration.");
            }
        } catch (\Exception $e) {
            die($e);
        }
    }
}
