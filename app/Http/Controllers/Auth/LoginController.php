<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        if ($request->session()->get('login') != null) {
            return redirect('/form');
        }

        return view('page.login');
    }

    public function authenticate(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        try {
            $client = new Client();
            $response = $client->request('POST', 'https://map.bpkp.go.id/api/v3/login', [
                'headers' => ['Content-type' => 'application/x-www-form-urlencoded'],
                'form_params' => [
                    'username' => $request->username,
                    'password' => $request->password,
                    'kelas_user' => 0
                ]
            ]);

            if ($response->getBody()) {
                $request->session()->regenerate();
                $request->session()->put('login', json_decode($response->getBody()));
                return redirect()->intended('/form');
            }
        } catch (BadResponseException  $e) {
            $response = $e->getResponse();

            if ($response->getBody()) {
                return redirect('/login')->with(['danger' => 'Login Gagal ! Periksa kembali username dan password.']);
            }
        }
    }

    public function logout(Request $request)
    {
        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/login');
    }
}
