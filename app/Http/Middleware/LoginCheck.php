<?php

namespace App\Http\Middleware;

use Closure;
// use GuzzleHttp\Client;
// use GuzzleHttp\Exception\BadResponseException;

class LoginCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->session()->get('login') == null || !$request->session()->get('login')->success) {
            return redirect('/login');
        }

        $response = $next($request);
        return $response->header('Cache-Control', 'nocache, no-store, max-age=0, must-revalidate')
            ->header('Pragma', 'no-cache')
            ->header('Expires', 'Sun, 02 Jan 1990 00:00:00 GMT');
    }
}
