<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UsersDetail extends Model
{
    protected $connection = 'default';

    protected $table = 'users_detail';
    public $timestamps = false;
}
