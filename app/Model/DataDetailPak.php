<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DataDetailPak extends Model
{
    protected $connection = 'default';

    protected $table = 'users_detail_pak';
    public $timestamps = false;
}
