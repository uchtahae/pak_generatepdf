<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DetailDupak extends Model
{
    protected $connection = 'default';

    protected $table = 'dupak_detail';
    public $timestamps = false;
}
