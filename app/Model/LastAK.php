<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LastAK extends Model
{
    protected $connection = 'default';

    protected $table = 'last_ak';
    public $timestamps = false;
}
