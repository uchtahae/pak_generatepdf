<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EvaluasiHistory extends Model
{
    protected $connection = 'default';

    protected $table = 'evaluasi_pak_history';
    public $timestamps = false;
}
