<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAK extends Model
{
    protected $connection = 'default';

    protected $table = 'user_angkre';
    public $timestamps = false;
}
