@extends('layout.fpage')
@section('title', 'Cetak PAK')

@section('container')

<section class="callout">
    <div class="container ">
        <h2 class="mb-5">Cetak PAK Pranata Komputer</h2>
    </div>

    <div class="container">
      <form action="/form/search" method="post">
        @csrf
        <div class="form-group row">
          <label for="nip" class="col-sm-2 col-form-label">Nama</label>
          <div class="col-lg-6">
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" placeholder="Masukkan Keyword Nama..." autocomplete="off" autofocus>
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
          </div>
        </div>
        {{-- <div class="form-group row mt-3">
          <label for="nip" class="col-sm-2 col-form-label">Tahun</label>
          <div class="col-lg-6">
            <select class="form-select" id="tahun" name="tahun" required>
              <option>Pilih Tahun</option>

              @php
                  $tahun = App\Model\UsersDetail::select('tahun_dibuat')
                  ->distinct()
                  ->get();
              @endphp
              
              @foreach($tahun as $thn)
                <option value="{{$thn->tahun_dibuat}}">{{$thn->tahun_dibuat}}</option>
              @endforeach
                
            </select>
          </div>
        </div> --}}
        <div class="form-group row mt-3">
          <div class="col-sm-10">
            <button type="submit" class="btn btn-primary" autocomplete="off"><i class="fas fa-search"></i> Cari Dokumen</button>
          </div>
        </div>
      </form>

      @if(isset($pegawai))
        @if(count($pegawai))
          <table class="table mt-3">
            <thead class="table-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Periode</th>
                <th scope="col">NIP</th>
                <th scope="col">Unit Kerja</th>
                <th scope="col">Pangkat</th>
                <th scope="col">Golongan</th>
                <th scope="col">Aksi</th>
              <tr>
            </thead>
            <tbody>
              @foreach( $pegawai as $pgw )
                  <tr>
                      <th scope="row">{{ $loop->iteration }}</th>
                      <td>{{ $pgw->nama_lengkap }}</td>
                      <td>{{ $pgw->periode }}</td>
                      <td>{{ $pgw->nip }}</td>
                      <td>{{ $pgw->unit_kerja }}</td>
                      <td>{{ $pgw->pangkat }}</td>
                      <td>{{ $pgw->gol_ruang }}</td>
                      <td>
                        <form action="{{ url( 'download/' . $pgw->id_pak ) }}" method="POST">
                          @csrf
                          <button type="submit" class="btn btn-outline-primary" target="_blank"><i class="fas fa-download"></i></button>
                        </form>
                      </td>
                  </tr>
              @endforeach
            </tbody>
          </table>

          {{-- <p>
            Halaman : {{ $pegawai->currentPage() }} <br/>
            Jumlah Data : {{ $pegawai->total() }} <br/>
          </p> --}}
          
          {{-- {{ $pegawai->links() }} --}}
        @else
          <div class="alert alert-warning mt-4" role="alert">
            <strong>Data Pegawai Tidak Ditemukan.</strong> periksa input nama kembali.
          </div>
        @endif
      @endif
    </div>
</section>
@endsection