<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>

  <body>
    <style type="text/css">
      table.pdftable {
        font-family: Arial, Helvetica, sans-serif;
        border: 1px solid #1C6EA4;
        width: 100%;
        text-align: left;
        border-collapse: collapse;
      }
      table.pdftable td, table.pdftable th {
        border: 1px solid #000000;
        padding: 1px 1px;
      }
      table.pdftable tbody td {
        font-size: 12px;
        color: #101010;
      }
      table.pdftable tr:nth-child(even) {
        background: #FFFFFF;
      }
      table.pdftable thead {
          border-bottom: 2px solid #444444;
      }
      table.pdftable thead th {
        font-size: 14px;
        font-weight: normal;
        text-align: center;
        color: #000000;
        border-left: 2px solid #D0E4F5;
      }
      table.pdftable thead th:first-child {
        border-left: none;
      }

      table.pdftable tfoot td {
        font-size: 12px;
      }
      table.pdftable tfoot .links {
        text-align: right;
      }
      table.pdftable tfoot .links a{
        display: inline-block;
        background: #1C6EA4;
        color: #FFFFFF;
        padding: 2px 8px;
        border-radius: 5px;
      }
    </style>

    <center>
      <p class="text-center">
        BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN
        PENETAPAN ANGKA KREDIT JABATAN FUNGSIONAL PRANATA KOMPUTER
        {{ $header['nomor_pak']}}
        {{ $header['masa_penilaian'] }}
      </p>
    </center>

    <div class="container">
        <table class="pdftable">
          <tr>
              <tr><td colspan="3" style="text-align: center;">KETERANGAN PERORANGAN</td></tr>
            <tbody>
              <tr>
                <td style="width: 10px">1</td>
                <td>Nama</td>
                <td>{{ $data_pak['nama_lengkap'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">2</td>
                <td>NIP</td>
                <td>{{ $data_pak['nip'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">3</td>
                <td>Nomor Seri Karpeg</td>
                <td>{{ $data_pak['karpeg'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">4</td>
                <td>Pangkat/Gol. Ruang/TMT</td>
                <td>{{ $data_pak['pangkat'] }}, {{ $data_pak['gol_ruang'] }}, {{ $data_pak['tmt'] }} </td>
              </tr>
              <tr>
                <td style="width: 10px">5</td>
                <td>Tempat dan Tanggal Lahir</td>
                <td>{{ $data_pak['tempat_lahir'] }}, {{ $data_pak['tgl_lahir'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">6</td>
                <td>Jenis Kelamin</td>
                <td>{{ $data_pak['jk'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">7</td>
                <td>Pendidikan Tertinggi</td>
                <td>{{ $data_pak['nama_strata'] }}, {{ $data_pak['nama_jurusan'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">8</td>
                <td>Jabatan Fungsional/TMT</td>
                <td>{{ $data_pak['jabatan'] }}</td>
              </tr>
              <tr>
                <td style="width: 10px">9</td>
                <td>Unit Kerja</td>
                <td>{{ $data_pak['unit_kerja'] }}</td>
              </tr>
            </tbody>
          </tr>

          <tr>
            <tr><td colspan="3" style="text-align: center;"> PENETAPAN ANGKA KREDIT </td></tr>
            <tr>
              <th>No</th>
              <th>Uraian</th>
              <th>Lama</th>
              <th>Baru</th>
              <th>Jumlah</th>
              <th>Angka Kredit untuk Kenaikan Pangkat</th>
            <tr>
            <tbody>
              @foreach( $detail as $dt )
                <tr>
                  <th scope="row">{{ $loop->iteration }}</th>
                  <td>{{ $dt['group_unsur'] }}</td>
                  <td>{{ $dt['lama'] }}</td>
                  <td>{{ $dt['baru'] }}</td>
                  <td>{{ $dt['jumlah'] }}</td>
                  <td> >=64 </td>
                </tr>
              @endforeach
            </tbody>
          </tr>
      </table>
    </div>
  </body>
</html>
