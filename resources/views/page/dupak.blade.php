@extends('layout.fpage')
@section('title', 'Cetak Surat Keterangan')

@section('container')

<section class="callout">
    <div class="container ">
        <h2 class="mb-5">Cetak Suket Penilaian Dupak</h2>
    </div>

    <div class="container">
      <form action="/dupak/search" method="post">
        @csrf
        <div class="form-group row">
          <label for="nama" class="col-sm-2 col-form-label">Penilai</label>
          <div class="col-lg-6">
            <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" 
            placeholder="Masukkan Keyword Nama..." autocomplete="off" autofocus>
            @error('nama')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
          </div>
        </div>
        <div class="form-group row mt-3">
          <label for="periode" class="col-sm-2 col-form-label">Periode</label>
          <div class="col-lg-6">
            <select class="form-select" id="periode" name="periode" selected="selected">
              <option value="">Pilih Periode</option>
              
              @foreach($periode as $prd)
                <option value="{{$prd->periode}}">{{$prd->periode}}</option>
              @endforeach
                
            </select>
            {{-- @error('periode')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror --}}
          </div>
        </div>

        <div class="form-group row mt-3">
          <div class="col-sm-10">
            <button type="submit" name="find" class="btn btn-primary" autocomplete="off"><i class="fas fa-search"></i> Cari Dokumen</button>
          </div>
        </div>
      </form>

      @if(isset($penilai))
        @if(count($penilai))
          <table class="table mt-3">
            <thead class="table-dark">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Periode</th>
                <th scope="col">Penilai</th>
                <th scope="col">NIP</th>
                <th scope="col">Gol/Ruang</th>
                <th scope="col">Jabatan</th>
                <th scope="col">Unit Kerja</th>
                <th scope="col">Aksi</th>
              <tr>
            </thead>
            <tbody>
              @foreach( $penilai as $pnl )
                  <tr>
                      <th scope="row">{{ $loop->iteration }}</th>
                      <td>{{ $pnl->periode }}</td>
                      <td>{{ $pnl->penilai }}</td>
                      <td>{{ $pnl->nip }}</td>
                      <td>{{ $pnl->gol_ruang }}</td>
                      <td>{{ $pnl->jabatan }}</td>
                      <td>{{ $pnl->unit_kerja }}</td>
                      <td>
                        {{-- <form action="{{ url( 'suket' , ['id_penilai' => $pnl->id_penilai,'id_periode' => $pnl->id_periode] ) }}" method="POST"> --}}
                          {{-- @csrf --}}
                          {{-- <button type="submit" class="btn btn-outline-primary" target="_blank"><i class="fas fa-download"></i></button> --}}
                          <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#nomorSuket" data-bs-whatever="@nomor_suket"><i class="fas fa-download"></i></button>

                        {{-- </form> --}}
                      </td>
                  </tr>
              @endforeach
            </tbody>
          </table>
        @else
          <div class="alert alert-warning mt-4" role="">
              <strong>Data Penilai Tidak Ditemukan.</strong> periksa input nama dan periode kembali.
          </div>
        @endif
      @endif
    </div>
</section>

<div class="modal fade" id="nomorSuket" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Masukkan Nomor Surat Keterangan</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="/suket" method="POST">
          @csrf
          @if(isset($penilai))
            @foreach($penilai as $pnl)
              <input type="hidden" class="form-control" id="id_penilai" name="id_penilai" value="{{ $pnl->id_penilai }}">
              <input type="hidden" class="form-control" id="id_periode" name="id_periode" value="{{ $pnl->id_periode }}">
            @endforeach
          @endif
            <div class="mb-3">
              <label for="recipient-name" class="col-form-label">No. Surat Keterangan</label>
              <input type="text" class="form-control" id="no_suket" name="no_suket" autocomplete="off" required>
              <div class="form-text">Contoh. NOMOR: KET-05/IP3/2021</div>
            </div>
          </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-bs-dismiss="modal">Close</button>
              <button type="submit" target="_blank" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
  </div>
</div>
@endsection