@extends('layout.flogin')
@section('title', 'Login')

@section('container')

<section class="vh-100">
    <div class="container-fluid h-custom">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col-md-9 col-lg-6 col-xl-5">
          <img src="https://mdbootstrap.com/img/Photos/new-templates/bootstrap-login-form/draw2.png" class="img-fluid"
            alt="Sample image">
        </div>
        <div class="col-md-8 col-lg-6 col-xl-4 offset-xl-1">
            <h2 class="mb-4">Login</h2>

            @if(session('danger'))
                <div class="alert alert-danger alert-dismissible fade show col-lg-8" role="alert">
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button> 
                    {{ session('danger') }}
                </div>
            @endif

            <form action="/login" method="POST">
                @csrf
                <div class="form-outline mb-4">
                    <label for="username" class="form-label">Username</label>
                    <input type="text" class="form-control @error('username') is-invalid @enderror" id="username" name="username" placeholder="Username WARGA" value="{{ old('username') }}" autocomplete="off" autofocus>
                    @error('username')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                
                <div class="form-outline mb-3">
                    <label for="password" class="form-label">Password</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password">
                    @error('password')
                        <div class="invalid-feedback">
                            {{ $message }}
                        </div>
                    @enderror
                </div>

                <div class="text-center text-lg-start mt-4 pt-2">
                    <button type="submit" class="btn btn-primary btn-md"
                      style="padding-left: 2.5rem; padding-right: 2.5rem;" name="submit">Login</button>
                </div>
            </form>
        </div>
      </div>
    </div>
    {{-- <img src="{{ asset('img/meeting.jpg') }}" alt="teamwork"> --}}
@endsection