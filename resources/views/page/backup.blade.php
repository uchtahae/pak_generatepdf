<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>

  <body>
    <style type="text/css">
      table tr td,
      table tr th{
        font-size: 9pt;
      }
    </style>

    <center>
      <p class="text-center">BADAN PENGAWASAN KEUANGAN DAN PEMBANGUNAN</p>
      <p class="text-center">PENETAPAN ANGKA KREDIT JABATAN FUNGSIONAL PRANATA KOMPUTER</p>
      {{-- {{ $detail['nomor_pak'] }}
      {{ $detail['masa_penilaian'] }} --}}
    </center>

    <div class="container">
      <table class="table table-bordered mt-3">
        <thead>
          <tr><td class="text-center" colspan="3">KETERANGAN PERORANGAN</td></tr>
        </thead>
        <tbody>
          @foreach( $perorangan as $pr )
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td scope="col">Nama</td>
                <td>{{ $pr->nama_lengkap }}</td>
            </tr>
            <tr>
                <td scope="col">NIP</td>
                <td>{{ $pr->nip }}</td>
              </tr>
              <tr>
                <td scope="col">Nomor Seri Karpeg</td>
                <td>{{ $pr->karpeg }}</td>
              </tr>
              <tr>
                <td scope="col">Pangkat/Gol. Ruang/TMT</td>
                <td>{{ $pr->pangkat }}, {{ $pr->golongan }}, {{ $pr->tmt }} </td>
              </tr>
              <tr>
                <td scope="col">Tempat dan Tanggal Lahir</td>
                <td>{{ $pr->tempat_lahir }}, {{ $pr->tgl_lahir }}</td>
              </tr>
              <tr>
                <td scope="col">Jenis Kelamin</td>
                <td>{{ $pr->jk }}</td>
              </tr>
              <tr>
                <td scope="col">Pendidikan Tertinggi</td>
                <td>{{ $pr->strata }}, {{ $pr->jurusan }}</td>
              </tr>
              <tr>
                <td scope="col">Jabatan Fungsional/TMT</td>
                <td>{{ $pr->jabatan }}</td>
              </tr>
              <tr>
                <td scope="col">Unit Kerja</td>
                <td>{{ $pr->unit_kerja }}</td>
            </tr>
          @endforeach
        </tbody>
      </table>
      {{-- <table>
        <thead class="table-dark">
          <tr><td class="text-center" colspan="6">PENETAPAN ANGKA KREDIT</td></tr>
          <tr>
              <th scope="col">No</th>
              <th scope="col">Uraian</th>
              <th scope="col">Lama</th>
              <th scope="col">Baru</th>
              <th scope="col">Jumlah</th>
              <th scope="col">Angka Kredit untuk Kenaikan Pangkat</th>
          <tr>
        </thead>
        <tbody>
            @foreach( $detail as $dt )
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ $dt->group_unsur }}</td>
                    <td>{{ $dt->lama }}</td>
                    <td>{{ $dt->baru }}</td>
                    <td>{{ $dt->jumlah }}</td>
                    <td>>=64</td>
                </tr>
            @endforeach
        </tbody>
      </table> --}}
    </div>
  </body>
</html>
