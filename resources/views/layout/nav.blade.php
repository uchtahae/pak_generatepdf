<nav id="sidebar-wrapper">
    <ul class="sidebar-nav">
        <li class="sidebar-brand">
            <img src="{{ asset('img/BPKP_Logo.png') }}" style="height: 2rem;">&nbsp;
            <a href="#page-top">PAK JFPK</a>
        </li>
        {{-- <li class="sidebar-nav-item"><a href="{{ url('/') }}">Welcome, Wahyu Wimasakunti</a></li> --}}
        <li class="sidebar-nav-item"><a href="{{ url('/form') }}"><i class="far fa-file-pdf"></i></i>&nbsp; Cetak PAK</a></li>
        <li class="sidebar-nav-item"><a href="{{ url('/dupak') }}"><i class="far fa-file-pdf"></i></i>&nbsp; Suket Penilaian Dupak</a></li>
        <li class="sidebar-nav-item">
            <a href="{{ url('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i>&nbsp; Logout </a>
            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>
    </ul>
</nav>