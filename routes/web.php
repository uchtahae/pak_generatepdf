<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can  register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// index dan login
Route::get('/', 'PagesController@home');
Route::get('/login', 'Auth\LoginController@login')->name('login');
Route::post('/login', 'Auth\LoginController@authenticate');
Route::post('/logout', 'Auth\LoginController@logout')->name('logout');

Route::middleware(['LoginCheck'])->group(function () {
    // menu dan generate pdf
    Route::get('/form', 'PagesController@form');
    Route::post('/form/search', 'PagesController@search');
    Route::post('/download/{id_pak}', 'DownloadController@pak');

    Route::get('/dupak', 'PagesController@dupak');
    Route::post('/dupak/search', 'PagesController@search_dupak');
    Route::post('/suket', 'DownloadController@suket');
});

// tester
Route::get('/pdf', 'PdfController@tester');
Route::get('/suket', 'PdfController@test_suket');
Route::get('/query', 'QueryController@test');
Route::get('/update', 'UpdateController@update');
Route::get('/old', 'QueryController@old');
Route::get('/config', 'PagesController@config');
